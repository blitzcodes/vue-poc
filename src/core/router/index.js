import Vue from 'vue';
import Router from 'vue-router';
import Hello from '@/components/04-pages/hello/hello.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'hello',
      component: Hello,
    },
  ],
});
