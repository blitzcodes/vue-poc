import aLink from '@/components/01-atoms/link/link.vue';

export default {
  name: 'm-link-list',
  components: {
    aLink,
  },
  props: {
    links: {
      type: Array,
      default() {
        return [];
      },
    },
  },
  mounted() {
    console.log('link-list', this);
  },
};
