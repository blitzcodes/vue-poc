export default {
  name: 'a-title',
  props: {
    level: {
      type: Number,
      default: 2,
    },
    label: String,
  },
  computed: {
    tag() {
      return `h{this.level}`;
    },
  },
  mounted() {
    console.log('title', this);
  },
};
