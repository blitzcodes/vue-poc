export default {
  name: 'a-link',
  props: {
    href: String,
    label: String,
    target: {
      type: String,
      default: '_self',
    },
  },
  mounted() {
    console.log('link', this);
  },
  methods: {
    hijack(e) {
      alert(`Nah ahh ahh, you didn't say the magic word!`);
      e.preventDefault();
      return false;
    },
  },
};
