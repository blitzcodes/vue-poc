import mLinkList from '@/components/02-molecules/link-list/link-list.vue';
import aTitle from '@/components/01-atoms/title/title.vue';

export default {
  name: 'p-hello-world',
  components: {
    mLinkList,
    aTitle,
  },
  data() {
    return {
      msg: 'Welcome to Your Vue.js PWA. ',
      links: {
        essential: [
          {
            href: 'https://vuejs.org',
            label: 'Core Docs',
            target: '_blank',
            rel: 'noopener',
          },
          {
            href: 'https://forum.vuejs.org',
            label: 'Forum',
            target: '_blank',
            rel: 'noopener',
          },
          {
            href: 'http://chat.vuejs.org',
            label: 'Chat',
            target: '_blank',
            rel: 'noopener',
          },
          {
            href: 'https://twitter.com/vuejs',
            label: 'Twitter',
            target: '_blank',
            rel: 'noopener',
          },
          {
            href: 'http://vuejs-templates.github.io/webpack/',
            label: 'Docs for This Template',
            target: '_blank',
            rel: 'noopener',
          },
        ],
        ecosystem: [
          {
            href: 'http://router.vuejs.org/',
            label: 'vue-router',
            target: '_blank',
            rel: 'noopener',
          },
          {
            href: 'https://vuex.vuejs.org',
            label: 'vuex',
            target: '_blank',
            rel: 'noopener',
          },
          {
            href: 'http://vue-loader.vuejs.org',
            label: 'vue-loader',
            target: '_blank',
            rel: 'noopener',
          },
          {
            href: 'https://github.com/vuejs/awesome-vue',
            label: 'awesome-vue',
            target: '_blank',
            rel: 'noopener',
          },
        ],
      },
    };
  },
};
